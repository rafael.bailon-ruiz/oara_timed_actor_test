#!/usr/bin/env python3

#  Copyright (c) ONERA 2022
#  Author: Rafael Bailon-Ruiz
import time
import rclpy.client
import rclpy.logging
import rclpy.parameter
import oara.observers
from std_srvs.srv import SetBool
from std_msgs.msg import Int32, Float32

class ObserverCallData(oara.observers.Observer):
    """Onboard memory observer"""

    def __init__(self, name: str):
        super().__init__(name)
        self.add_data('information', Int32)
        self.add_function('information', 'load', SetBool, self.information_load)

    def information_load(self, data: Int32, request: SetBool.Request):
        response = SetBool.Response(success=True, message="Hello")
        self.get_logger().info(f"information_load request: {request}")
        self.get_logger().info(f"information_load response: {str(response)}")
        return response



if __name__ == '__main__':
    oara.main(ObserverCallData, "observer_calldata", loglevel=rclpy.logging.LoggingSeverity.INFO)