#!/usr/bin/env python3

#  Copyright (c) ONERA 2022
#  Author: Rafael Bailon-Ruiz

import typing as ty

import rclpy.logging
import rclpy.time
import rclpy.timer
from oara_std_interfaces.msg import EmptyGoal, StringGoal
from std_msgs.msg import Empty, String, Int32
from std_srvs.srv import SetBool

import oara
from oara.actors.timed_pos_actor import TimedPOSActor
from oara_interfaces.msg import GoalRequest, ResolveTo

import random


class TimedActorTest(TimedPOSActor):
    """TimedActorTest"""

    def __init__(self, name: str):
        super().__init__(name, EmptyGoal, select_unique=False)
        self.add_child_actor('printerx1', StringGoal)
        self.add_child_actor('printerx2', StringGoal)
        self.add_data("information", Int32)

    def on_configure(self, state):
        self.set_parameters([rclpy.parameter.Parameter(
            "oara.model.monitoring_period", rclpy.parameter.Parameter.Type.DOUBLE, 1.0)])
        return super().on_configure(state)

    def on_activate(self, state):
        return super().on_activate(state)

    def on_deactivate(self, state):
        return super().on_deactivate(state)

    def select(self, goal_id: oara.UID, goal: Empty,
               # report: ty.Optional[oara.DispatchReport],
               # event: ty.Optional[oara.Event]
               ):
        return True

    def expand(self, goal_id: oara.UID, goal: Empty,
               # report: ty.Optional[oara.DispatchReport],
               # event: ty.Optional[oara.Event]
               ):
        self.get_logger().info(f"{goal_id} {goal}")
        now = self.get_clock().now()
        plan = oara.Plan()

        result_call = self.call_data("information", "load", SetBool, SetBool.Request(data=True))
        self.get_logger().info(f"{result_call}")

        plan.add_task(0, String(data="Wait for it..."), 'printerx1', now + rclpy.time.Duration(seconds=0))
        plan.add_task(1, String(data="Wait for it! (x2)"), 'printerx2', now + rclpy.time.Duration(seconds=20))
        # plan.add_task(2, String(data="I sait wait!!"), 'printerx1', now + rclpy.time.Duration(seconds=5))
        plan.add_task(3, String(data="Done :]"), 'printerx1', now + rclpy.time.Duration(seconds=30))
        # plan.add_task(4, String(data="Done :3 (x2)"), 'printerx2', now + rclpy.time.Duration(seconds=90))

        # plan.add_precedence(0, 1)
        # plan.add_precedence(1, 2)
        plan.add_precedence(1, 3)
        # plan.add_precedence(2, 4)

        self.add_plan(goal_id, plan, len(plan))
        return True

    def evaluate(self, gid, *args, **kwargs) -> ResolveTo:
        self.get_logger().info(f"Evaluate ({gid}, {args}, {kwargs})")
        # return random.choice([ResolveTo.CONTINUE, ResolveTo.REFORM, ResolveTo.DEFER, ResolveTo.REPLAN, ResolveTo.REPAIR])
        return random.choice([ResolveTo.CONTINUE])

    def drop(self, goal_id):
        self.get_logger().info(f"Drop {goal_id})")
        return super().drop(goal_id)


if __name__ == '__main__':
    oara.main(TimedActorTest, "timed_actor_test", loglevel=rclpy.logging.LoggingSeverity.INFO)
