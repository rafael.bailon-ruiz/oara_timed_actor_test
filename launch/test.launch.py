#  Copyright (c) ONERA 2022
#  Author: Rafael Bailon-Ruiz

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import LifecycleNode


def generate_launch_description():
    info_level = ['--ros-args', '--log-level', 'INFO']
    debug_level = ['--ros-args', '--log-level', 'DEBUG']

    observer_calldata = LifecycleNode(
        package='oara_timed_actor_test',
        executable='observer_calldata.py',
        name='observer_calldata',
        output="both",
        arguments=info_level,
        parameters=[{}],
        namespace="/")

    timed_actor = LifecycleNode(
        package='oara_timed_actor_test',
        executable='timed_actor_test.py',
        name='timed_actor_test',
        output="both",
        arguments=info_level,
        parameters=[{
            'printerx1.actor': 'printerx1_controller',
            'printerx2.actor': 'printerx2_controller',
            'information.observer': 'observer_calldata',
            'information.data': 'information',
            }],
        namespace="/")

    printerx1_controller = LifecycleNode(package='oara',
                                       executable='print_controller.py',
                                       name='printerx1_controller',
                                       namespace="/",
                                       output="both",
                                       arguments=["oara_std_interfaces/StringGoal"])

    printerx2_controller = LifecycleNode(package='oara',
                                       executable='print_controller.py',
                                       name='printerx2_controller',
                                       namespace="/",
                                       output="both",
                                       arguments=["oara_std_interfaces/StringGoal"])

    return LaunchDescription([
        observer_calldata,
        timed_actor,
        printerx1_controller,
        printerx2_controller
    ])
