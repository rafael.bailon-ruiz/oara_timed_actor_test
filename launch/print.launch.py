#  Copyright (c) ONERA 2022
#  Author: Rafael Bailon-Ruiz

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess
from launch.substitutions import LaunchConfiguration
from launch_ros.actions import LifecycleNode


def generate_launch_description():
    info_level = ['--ros-args', '--log-level', 'INFO']
    debug_level = ['--ros-args', '--log-level', 'DEBUG']

    printerx1_controller = LifecycleNode(package='oara',
                                       executable='print_controller.py',
                                       name='printerx1_controller',
                                       namespace="/",
                                       output="both",
                                       arguments=["oara_std_interfaces/StringGoal", "--sleep", "5.0"])

    return LaunchDescription([
        printerx1_controller,
    ])
